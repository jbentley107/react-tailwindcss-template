/**
 * @file setMessagesAndColors.js
 */

// Import dependencies
import React, { useEffect, useState, setIsLoaded, setError } from 'react'

export default function useMessagesAndColors(messages, status, statusSettersObject) {
  if (status === 0) {
    statusSettersObject.statusMessage(messages[0]);
    statusSettersObject.textClass('text-status-green');
    statusSettersObject.backgroundClass('bg-status-green');
  } else if (status === 1) {
    statusSettersObject.statusMessage(messages[1]);
    statusSettersObject.textClass('text-status-yellow');
    statusSettersObject.backgroundClass('bg-status-yellow');
  } else if (status === 2) {
    statusSettersObject.statusMessage(messages[2]);
    statusSettersObject.textClass('text-status-red');
    statusSettersObject.backgroundClass('bg-status-red');
  }
}