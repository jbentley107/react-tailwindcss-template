/** 
 * @file App.js
 */

// Import dependencies
import React, { useState, useEffect, setIsLoaded, setItems, setError } from 'react';
import '../styles/App.css';

// Import components
import HeaderBar from '../components/HeaderBar/HeaderBar';
import FooterBar from '../components/FooterBar/FooterBar';
import Layout from '../components/Layout/Layout';

function App() {
  /**
   * States
   */
  // Data fetching
  const [error, setError] = useState(null); // null
  const [isLoaded, setIsLoaded] = useState(false); // true
  const [messages, setMessages] = useState({});
  const [overallStatus, setOverallStatus] = useState([]);
  const [departments, setDepartments] = useState([]);

  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {    
    // Get the messages
    fetch("http://localhost:3000/messages")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setMessages(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )

    // Get the overall status
    fetch("http://localhost:3000/overall")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setOverallStatus(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )

    // Get the departments
    fetch("http://localhost:3000/departments")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setDepartments(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])

  return (
    <div className="App">
      <HeaderBar 
        applicationTitle={'Enhanced Occupancy and Notification status'}
      />
      <Layout 
        currentStatus={overallStatus.status} // Passing in a number 0-2
        numVisitors={overallStatus.numVisitors}
        maxOccupancy={overallStatus.maxOccupancy}
        messages={messages} // Passing in a messges object
        departments={departments} // Passing in an array of departments
      />
      <FooterBar />
    </div>
  );
}

export default App;
