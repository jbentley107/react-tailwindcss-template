/** 
 * @file index.js
 */

// Import dependencies
import React from 'react';
import ReactDOM from 'react-dom';
// Import CSS
import './styles/index.css';
import './styles/tailwind.generated.css';
// Import components
import App from './pages/App';
import * as serviceWorker from './serviceWorker';
// Import typefaces
import 'typeface-roboto';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
