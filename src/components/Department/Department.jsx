/**
 * @file Department.js
 */
// Import dependencies
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion';

// Import components
import DepartmentSubsection from '../DepartmentSubsection/DepartmentSubsection';
import CameraFeed from '../CameraFeed/CameraFeed';

// Import assets
import {ReactComponent as Person} from '../../assets/SVG/fi-male.svg';

const Department = (props) => {
  const {
    departmentName,
    departmentStatus,
    numVisitors,
    maxOccupancy,
    subsections,
    messages,
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  // Subsection states
  const [subsectionStatus, setSubsectionStatus] = useState('red');
  const [subsectionStatusMessage, setSubsectionStatusMessage] = useState('Unsafe distancing');  

  const [statusTextClass, setStatusTextClass] = useState('text-gray-subheading');
  const [statusBackgroundClass, setStatusBackgroundClass] = useState('bg-gray-subheading');
  const [statusMessage, setStatusMessage] = useState('pending');

  useEffect(() => {
    if (departmentStatus === 0) {
      setStatusMessage(messages.green);
      setStatusTextClass('text-status-green');
      setStatusBackgroundClass('bg-status-green');
    } else if (departmentStatus === 1) {
      setStatusMessage(messages.yellow);
      setStatusTextClass('text-status-yellow');
      setStatusBackgroundClass('bg-status-yellow');
    } else if (departmentStatus === 2) {
      setStatusMessage(messages.red);
      setStatusTextClass('text-status-red');
      setStatusBackgroundClass('bg-status-red');
    } 
  }, [departmentStatus])

  function Subsections () {    
    return (
      // Go through the array of departments objects
      subsections.map((subsection) =>
        <AccordionItemPanel>
          {/* // Render a subsection */}
          <DepartmentSubsection 
            key={subsection.id}
            subsectionName={subsection.name}    
            subsectionStatus={subsection.status}
            numVisitors={subsection.numVisitors}      
            maxOccupancy={subsection.maxOccupancy}
            messages={messages}     
          />
        </AccordionItemPanel>
      )
    )
  };

  return (
    <Tag className={`department department--${variant} ${className}`}>      
      {/* Everything below should be in a collapsible accordion */}
      <Accordion allowZeroExpanded={true}>
        <AccordionItem>
          <AccordionItemHeading>
            <AccordionItemButton>
              <div className={`department__department-name-row flex sm:pl-8 sm:py-4 md:pl-32 bg-white flex`}>
                <span className={`department__department-status-icon ${statusBackgroundClass} flex-initial rounded-full sm:w-12 sm:h-12 sm:text-3xl font-bold text-white`}>!</span>
                <div className={`department__department-name-status-container flex-initial w-8/12`}>
                  <h2 className={`department__department-name align-middle text-left flex-initial sm:pl-4 md:pr-8 font-bold flex-initial`}>{departmentName}</h2>
                  <p className={`department__department-status-message text-left sm:pl-4 md:pr-8`}>{statusMessage}</p>
                </div>
                <div className={`department__person-icon flex-initial`}>
                  <Person />
                </div>        
                <p className={`department__department-current-visitors flex-initial`}>{numVisitors}/</p>
                <p className={`department__department-max-occupancy flex-initial`}>{maxOccupancy}</p>
                {/* <span className="department__horizontal-line align-middle w-full h-1 bg-nttsilver ml-8"></span> */}
              </div>
            </AccordionItemButton>
          </AccordionItemHeading>
          <Subsections />          
        </AccordionItem>
      </Accordion>
        {children}
      <style jsx>{`
        .department {

        }
      `}</style>
    </Tag>
  )
}

Department.propTypes = {
  departmentName: PropTypes.string,
  departmentStatus: PropTypes.number,
  numVisitors: PropTypes.number,
  maxOccupancy: PropTypes.number,
  subsections: PropTypes.array,
  messages: {'green': 'Safe distancing', 'yellow': 'Approaching unsafe distancing', 'red': 'unsafe distancing'},
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

Department.defaultProps = {
  departmentName: 'Department Name',
  departmentStatus: 3,
  numVisitors: 0,
  maxOccupancy: 20,
  subsections: [],
  messages: {'green': 'Safe distancing', 'yellow': 'Approaching unsafe distancing', 'red': 'unsafe distancing'},
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default Department
