# StatusIcon

## Props

Property | Values | Default | Description
--- | --- | --- | ---
`status` | `[String]` Any | `'passed'` | Defines the current status of the visitor using the health check.
`tagName` | `[String]` Any | `'div'` | Defines the HTML tagName output by the tag.
`className` | `[String]` Any | `''` | Defines arbitrary className to add to the component's class list.
`variant` | `[String]` Any | `'default'` | Defines BEM modifier to add to class list, e.g., `'status-icon--default'`.
`children` | `[Object]` | `undefined` | Defines the children elements passed to the component.
