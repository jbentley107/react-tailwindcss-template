/**
 * @file Layout.js
 */
// Import dependencies
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// Import components
import OverallStatus from '../OverallStatus/OverallStatus';
import Department from '../Department/Department';

const Layout = (props) => {
  const {    
    currentStatus,
    messages,
    numVisitors,
    maxOccupancy,
    departments,
    tagName: Tag,
    className,
    variant,
    children,
  } = props  

  function Departments () {    
    return (
      // Go through the array of departments objects
      departments.map((department) =>
        // Render a department
        <Department 
          key={department.id}
          departmentName={department.name}    
          departmentStatus={department.status}
          numVisitors={department.numVisitors}      
          maxOccupancy={department.maxOccupancy}      
          subsections={department.subsections}
        />
      )
    )
  };

  return (
    <Tag className={`layout layout--${variant} ${className}`}>
      {/* Overall occupancy level status */}
      <OverallStatus         
        currentStatus={currentStatus}
        numVisitors={numVisitors}
        maxOccupancy={maxOccupancy}
        messages={messages}
      />
      {/* Render the departments */}
      <Departments 
        messages={messages}
      />                     
      {children}
      <style jsx>{`
        .layout {

        }
      `}</style>
    </Tag>
  )
}

Layout.propTypes = {
  currentStatus: PropTypes.number,
  messages: PropTypes.object,
  departments: PropTypes.array,
  numVisitors: PropTypes.number,
  maxOccupancy: PropTypes.number,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

Layout.defaultProps = {
  currentStatus: 3,
  messages: {'green': 'Safe distancing', 'yellow': 'Approaching unsafe distancing', 'red': 'unsafe distancing'},
  departments: [],
  numVisitors: 0,
  maxOccupancy: 0,
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default Layout
