/**
 * @file DepartmentSubsection.js
 */
// Import dependencies
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// Import assets
import {ReactComponent as Person} from '../../assets/SVG/fi-male.svg';

const DepartmentSubsection = (props) => {
  const {
    subsectionName,
    subsectionStatus,
    subsectionStatusMessage,
    numVisitors,
    maxOccupancy,
    messages,
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  // States
  const [statusTextClass, setStatusTextClass] = useState('text-gray-subheading');
  const [statusBackgroundClass, setStatusBackgroundClass] = useState('bg-gray-subheading');
  const [statusMessage, setStatusMessage] = useState('pending');

  useEffect(() => {
    if (subsectionStatus === 0) {
      setStatusMessage(messages.green);
      setStatusTextClass('text-status-green');
      setStatusBackgroundClass('bg-status-green');
    } else if (subsectionStatus === 1) {
      setStatusMessage(messages.yellow);
      setStatusTextClass('text-status-yellow');
      setStatusBackgroundClass('bg-status-yellow');
    } else if (subsectionStatus === 2) {
      setStatusMessage(messages.red);
      setStatusTextClass('text-status-red');
      setStatusBackgroundClass('bg-status-red');
    } 
  }, [subsectionStatus])

  return (
    <Tag className={`department-subsection department-subsection--${variant} ${className}`}>
      <div className={`department-subsection__container bg-active-background`}>
        <div className={`department-subsection__container__information-row`}>
          <div className={`department-subsection__department-name-row flex sm:pl-12 sm:py-4 md:pl-32 bg-active-background flex`}>
            <span className={`department-subsection__department-status-icon ${statusBackgroundClass} flex-initial rounded-full sm:w-8 sm:h-8 sm:text-lg font-bold text-white`}>!</span>
            <div className={`department-subsection__department-name-status-container flex-initial w-8/12`}>
              <h2 className={`department-subsection__department-name align-middle text-left flex-initial sm:pl-4 md:pr-8 font-bold flex-initial`}>{subsectionName}</h2>
              <p className={`department-subsection__department-status-message text-left sm:pl-4 md:pr-8`}>{subsectionStatusMessage}</p>
            </div>
            <div className={`department-subsection__person-icon flex-initial`}>
              <Person />
            </div>        
            <p className={`department-subsection__department-current-visitors flex-initial`}>{numVisitors}/</p>
            <p className={`department-subsection__department-max-occupancy flex-initial`}>{maxOccupancy}</p>
            {/* <span className="department__horizontal-line align-middle w-full h-1 bg-nttsilver ml-8"></span> */}
          </div>
        </div>        
        <div className={`department-subsection__container__camera-feed`}>
          {children}
        </div>
      </div>
      <style jsx>{`
        .department-subsection {

        }
      `}</style>
    </Tag>
  )
}

DepartmentSubsection.propTypes = {
  subsectionName: PropTypes.string,
  subsectionStatus: PropTypes.string,
  subsectionStatusMessage: PropTypes.string,
  numVisitors: PropTypes.number,
  maxOccupancy: PropTypes.number,
  messages: PropTypes.object,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

DepartmentSubsection.defaultProps = {
  subsectionName: 'Subsection Name',
  subsectionStatus: 'green',
  subsectionStatusMessage: 'Safe distancing',
  numVisitors: 0,
  maxOccupancy: 15,
  messages: {'green': 'Safe distancing', 'yellow': 'Approaching unsafe distancing', 'red': 'unsafe distancing'},
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default DepartmentSubsection
