/**
 * @file OverallStatus.js
 */
// Import dependencies
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

// Import assets
import {ReactComponent as Person} from '../../assets/SVG/fi-male.svg';

const OverallStatus = (props) => {
  const {
    numVisitors,
    maxOccupancy,
    currentStatus,
    messages,
    tagName: Tag,
    className,
    variant,
    children,
  } = props   

  const [statusTextClass, setStatusTextClass] = useState('text-gray-subheading');
  const [statusBackgroundClass, setStatusBackgroundClass] = useState('bg-gray-subheading');
  const [statusMessage, setStatusMessage] = useState('pending');

  useEffect(() => {
    if (currentStatus === 0) {
      setStatusMessage(messages.green);
      setStatusTextClass('text-status-green');
      setStatusBackgroundClass('bg-status-green');
    } else if (currentStatus === 1) {
      setStatusMessage(messages.yellow);
      setStatusTextClass('text-status-yellow');
      setStatusBackgroundClass('bg-status-yellow');
    } else if (currentStatus === 2) {
      setStatusMessage(messages.red);
      setStatusTextClass('text-status-red');
      setStatusBackgroundClass('bg-status-red');
    } 
  }, [currentStatus])

  return (
    <Tag className={`overall-status overall-status--${variant} ${className}`}>
      <div className={`overall-status__status-row flex w-full`}>        
        <div className={`overall-status__info-container flex-initial text-left pl-8 py-4 bg-active-background w-full`}> 
          <div className={'overall-status__store-status-container flex'}>
            <span className={`overall-status__status-color ${statusBackgroundClass} flex-initial sm:w-8 sm:h-8 rounded-full align-middle`}></span>
            <h4 className={`overall-status__description-text text-md text-gray-subheading font-roboto align-middle flex-initial sm:pl-4`}>Store status</h4>
          </div>          
          <Person />
          <span className={`overall-status__number-visitors overall-status__number-visitors--${currentStatus} ${statusTextClass} font-roboto text-2xl font-bold align-middle`}>{numVisitors} / </span><span className={`status-row__max-occupancy text-bold text-2xl font-bold align-middle`}>{maxOccupancy}&nbsp;&nbsp;&nbsp;</span>
          <p><span className={`overall-status__status-statement overall-status__status-statement--${currentStatus} font-roboto text-xl align-middle`}>{statusMessage}</span></p>
        </div>
      </div>
      {children}
      <style jsx>{`
        .overall-status {

        }

        .overall-status_-status-row__status-icon {
          width: 50px;
          height: 50px;
          border: 1px solid #333;
        }
      `}</style>
    </Tag>
  )
}

OverallStatus.propTypes = {
  numVisitors: PropTypes.number,
  maxOccupancy: PropTypes.number,
  messages: PropTypes.object,
  currentStatus: PropTypes.number,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

OverallStatus.defaultProps = {
  numVisitors: 0,
  maxOccupancy: 100,
  messages: {'green': 'Safe distancing', 'yellow': 'Approaching unsafe distancing', 'red': 'unsafe distancing'},
  currentStatus: PropTypes.number,
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default OverallStatus; 
