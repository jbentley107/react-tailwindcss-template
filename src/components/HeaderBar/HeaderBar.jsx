/**
 * @file HeaderBar.js
 */
import * as React from 'react'
import PropTypes from 'prop-types'

// Import components
import {ReactComponent as NTTLogo} from '../../assets/SVG/ntt_logo.svg';

const HeaderBar = (props, { hasMenuButton }) => {
  const {
    applicationTitle,
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  if (!hasMenuButton) {
    return (
      <Tag className={`header-bar header-bar--${variant} ${className} h-16`}>
        <header className="bg-health-screening-header fixed inset-x-0 top-0 flex sm:h-18">
          <div className="ntt-logo w-2/12 pl-8 py-4 align-middle inline-block">
            <NTTLogo />
          </div>
          <h2 className="text-white font-semibold text-xl leading-snug inline-block text-left w-8/12 sm:pl-16 md:pl-8 md:py-4 align-middle">{applicationTitle}</h2>
        </header>
        {children}
        <style jsx>{`
          .header-bar {

          }

          .ntt-logo svg {
            width: 67px;
            height: 24px;
          }
        `}</style>
      </Tag>
    )
  }
}

HeaderBar.propTypes = {
  applicationTitle: PropTypes.string,
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node
}

HeaderBar.defaultProps = {
  tagName: 'Application Title',
  tagName: 'div',
  className: '',
  variant: 'default',
  children: ''
}

export default HeaderBar;
