/**
 * @file CameraFeed.js
 */
import * as React from 'react'
import PropTypes from 'prop-types'

const CameraFeed = (props) => {
  const {
    tagName: Tag,
    className,
    variant,
    children,
  } = props

  return (
    <Tag className={`camera-feed camera-feed--${variant} ${className}`}>
      {children}
      <style jsx>{`
        .camera-feed {

        }
      `}</style>
    </Tag>
  )
}

CameraFeed.propTypes = {
  tagName: PropTypes.string,
  className: PropTypes.string,
  variant: PropTypes.oneOf(['default']),
  children: PropTypes.node,
}

CameraFeed.defaultProps = {
  tagName: 'div',
  className: '',
  variant: 'default',
  children: '',
}

export default CameraFeed
